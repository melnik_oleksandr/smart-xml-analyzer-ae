package ua.khpi.melnik.analyzer;

import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ua.khpi.melnik.util.JsoupUtil;

import java.io.File;
import java.util.Optional;
import java.util.TreeMap;

/**
 * Class for analyzing changes on HTML pages after some changes and find specific element.
 * 
 * @author Oleksandr Melnyk
 */
public class XmlAnalyzer {

    /**
     * Parse origin html page and find all information about element to be found then find this element
     * with some changes in another html page.
     *
     * @param firstFile  path to first html file
     * @param secondFile path to second html file
     * @param elementId  id of element to be found
     */
    public static void parse(String firstFile, String secondFile, String elementId) {
        Optional<Element> elementFromFirstFile = JsoupUtil.findElementById(new File(firstFile), elementId);
        TreeMap<Integer, Element> elementMap = new TreeMap<>();

        if (elementFromFirstFile.isPresent()) {
            Element primaryElement = elementFromFirstFile.get();
            Attributes attributes = primaryElement.attributes();

            attributes.forEach(attribute -> {
                String cssQuery = JsoupUtil.constructCssQuery(primaryElement, attribute);
                Optional<Elements> elementFromSecondFile = JsoupUtil.findElementsByQuery(new File(secondFile), cssQuery);
                elementFromSecondFile.ifPresent(elements -> JsoupUtil.findElement(elementMap, primaryElement, elements));
            });

            if (!elementMap.isEmpty()) {
                Element element = elementMap.lastEntry().getValue();
                JsoupUtil.buildPath(element);
            }
        }
    }
}
